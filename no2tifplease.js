/*
 * Inspired from https://stackoverflow.com/questions/2497200/how-to-listen-for-changes-to-the-title-element
 *
 */
document.title = "";
var oldTitle = document.title;

window.setInterval(function()
		{
				if (document.title !== oldTitle)
				{
					document.title = "";
				}
				oldTitle = document.title;
		}, 100); //check every 100ms
